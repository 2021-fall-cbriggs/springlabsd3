<%@ include file="/WEB-INF/layouts/include.jsp" %>

<div class="container">
	<div class="row mt-4">
		<!-- <div class="hidden-xs col-sm-2 col-md-3 col-lg-4"></div> -->
		<div class="col-12 text-center d-flex justify-content-center">
	    	<div class="card card-default p-2">
				<div class="card-heading mx-auto">
	        		<header>
						<a href="https://teamcluster.oreillyauto.com/Home/main">
						  <!-- <img class="pt-2" src="https://idp.oreillyauto.com/idp/images/logo-sm.png" alt="Logo"> -->
						  <img class="pt-2" src="<%=request.getContextPath() %>/resources/img/logo-sm.png" alt="Logo">
						</a>
					</header>
				</div>
				<div class="card-body">
					<div>
						<form id="loginForm" name="loginForm" action="<c:url value='/loginProcess'/>" method="POST">
							<legend class="text-center">Log in</legend>          
							<div class="form-group ">
								<label class="control-label" for="username">Username</label>
								<input class="form-control" id="username" name="username" type="text" value="" />
							</div>
							<div class="form-group">
								<label class="control-label" for="password">Password</label>
								<input class="form-control" id="password" name="password" type="password" value="">
							</div>
							<div class="form-group text-center">
								<button class="login-btn btn btn-primary" type="submit" name="_eventId_proceed" id="login" onclick="this.childNodes[0].nodeValue='Logging in, please wait...'">Login</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!-- <div class="hidden-xs col-sm-2 col-md-3 col-lg-4"></div> -->
	</div>
	<style>
		.card-body {
			padding-bottom: 0;
		}
	</style>
</div>
