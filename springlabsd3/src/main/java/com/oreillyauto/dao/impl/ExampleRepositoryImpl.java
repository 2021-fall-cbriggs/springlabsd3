package com.oreillyauto.dao.impl;

import java.util.List;

import javax.persistence.Query;

import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import com.oreillyauto.dao.custom.ExampleRepositoryCustom;
import com.oreillyauto.domain.Example;
import com.oreillyauto.domain.QExample;
import com.oreillyauto.domain.QNickname;
import com.querydsl.jpa.JPAExpressions;

@Repository
public class ExampleRepositoryImpl extends QuerydslRepositorySupport implements ExampleRepositoryCustom {
	
	QExample exampleTable = QExample.example;
	QNickname nicknameTable = QNickname.nickname;
			
	public ExampleRepositoryImpl() {
		super(Example.class);
	}
	
    @SuppressWarnings("unchecked")
    @Override
    public List<Example> getExamples() {
		List<Example> eventList = (List<Example>) (Object) getQuerydsl().createQuery()
                .from(exampleTable)
                .fetch();
        return eventList;
    }
    
    @SuppressWarnings("unchecked")
    public void printExampleTable() {
        QExample exampleTable = QExample.example;
        
        List<Example> list = (List<Example>) (Object) getQuerydsl().createQuery()
                .from(exampleTable)
                .fetch();
        
        for (Object result : list) {
            System.out.println(result);
        }
    }
    
    public void printList(List<?> list) {
        for (Object result : list) {
            System.out.println(result);
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Example> getExampleById(Integer id) {
        String sql = "SELECT * " + 
                     "  FROM examples " + 
                     " WHERE id = ?";
        Query query = getEntityManager().createNativeQuery(sql);
        query.setParameter(1, id); // NOTICE PARAMETERS ARE "1-BASED" and not "0-based"
        return (List<Example>)query.getResultList();
    }
    
    
    public Example getFirstExample() {
    	QExample exampleTable = QExample.example;
		Example example = (Example) getQuerydsl().createQuery()
                .from(exampleTable)
                .limit(1)
                .fetch();
        return example;
    }
    
    @SuppressWarnings("unchecked")
    public List<Example> getAllExamples() {
    	QExample exampleTable = QExample.example;
		List<Example> eventList = (List<Example>) (Object) getQuerydsl().createQuery()
                .from(exampleTable)
                .fetch();
        return eventList;
    }
	
	@Override
	public void testQueries(String day) {
		switch (day) {
		case "3":
			testDayThree();
			break;
		case "4":
//			testDayFour();
			break;
		}
	}

	@SuppressWarnings("unchecked")
	public void testDayThree() {
		    	
    	// Example 1
    	// Simple example - retrieve one record 
		// This is the "LONG" way
//    	Example example = (Example) getQuerydsl().createQuery()
//    	           .from(exampleTable)
//    	           .limit(1)
//    	           .fetchOne();
//    	System.out.println("Example: " + example);

    	// -----------------------------------------------------------------

        // EXAMPLE 2
    	// Simple example - retrieve one record 
		// This is the "SHORT" way
//    	Example example2 = from(exampleTable)
//    		       .limit(1)
//    		       .fetchOne();
//    	System.out.println(example2);
    	
    	// -----------------------------------------------------------------    	
    	
    	// EXAMPLE 3
//    	List<Example> exampleList = from(exampleTable)
//                                  .fetch();
//    	Helper.printList(exampleList);		
            	
    	// -----------------------------------------------------------------  	
    	
        // EXAMPLE 4
        // java.persistence.Query takes a SQL string expression "SELECT * FROM <table>";
//		String sql = "SELECT * " + 
//					 "  FROM examples ";
//		Query query = getEntityManager().createNativeQuery(sql);
//		List<Example> exampleList = (List<Example>)(Object)query.getResultList();
//		Helper.printList(exampleList);

        // -----------------------------------------------------------------
        
        // EXAMPLE 5 
		// Select One "Jeffery"
//		Example exampleList = (Example) getQuerydsl().createQuery()
//					.from(exampleTable)
//					.where(exampleTable.firstName.eq("Jeffery"))
//					.fetchOne();
//        System.out.println(exampleList);
        
		// -----------------------------------------------------------------
		
		// EXAMPLE 6
		// Select users with first name = "Jeffery" 
//		List<Example> jefferyList = (List<Example>) (Object) getQuerydsl().createQuery()
//				.select(exampleTable.id, exampleTable.firstName, exampleTable.lastName)
//				.from(exampleTable)
//				.where(exampleTable.firstName.eq("Jeffery"))
//				.fetch();
//		Helper.printList(jefferyList);
        
		// -----------------------------------------------------------------
		
        // Example 7
		// Multiple sources (tables)
//        List<Object> exampleList = (List<Object>) (Object) getQuerydsl().createQuery()
//                .select(exampleTable.firstName, exampleTable.lastName, nicknameTable.nickName)
//                .from(exampleTable)
//                .innerJoin(nicknameTable)
//                .on(exampleTable.id.eq(nicknameTable.id))
//                .fetch();
//        Helper.printList(exampleList);
        
		// -----------------------------------------------------------------
		
        // Example 8
		// Multiple Filters - AND
//        List<Object> exampleList4 = (List<Object>) (Object) getQuerydsl().createQuery()
//                .select(exampleTable.firstName, exampleTable.lastName, nicknameTable.nickName)
//                .from(exampleTable)
//                .innerJoin(nicknameTable)
//                .on(exampleTable.id.eq(nicknameTable.id))
//                .where(exampleTable.firstName.equalsIgnoreCase("jeffery"), 
//                       (nicknameTable.nickName.equalsIgnoreCase("giraffe")))
//                .fetch();  // Comma in the predicate is an "and"
//        Helper.printList(exampleList4);
        
		// -----------------------------------------------------------------
        
        // Example 9
		// Multiple Filters - OR
//        List<Object> exampleList4a = (List<Object>) (Object) getQuerydsl().createQuery()
//                .select(exampleTable.firstName, exampleTable.lastName, nicknameTable.nickName)
//                .from(exampleTable)
//                .innerJoin(nicknameTable)
//                .on(exampleTable.id.eq(nicknameTable.id))
//                .where(
//                		nicknameTable.nickName.eq("giraffe")
//                		 .or(nicknameTable.nickName.eq("dawg"))
//                	  ) // another example using or instead of and
//                .fetch();
//        Helper.printList(exampleList4a);
		
		// -----------------------------------------------------------------
        
        // Example 10 
		// Left Join
//        List<Object> exampleList5 = (List<Object>) (Object) getQuerydsl().createQuery()
//                .select(exampleTable.firstName, exampleTable.lastName, nicknameTable.nickName)
//                .from(exampleTable)
//                .leftJoin(nicknameTable)
//                .on(exampleTable.id.eq(nicknameTable.id))
//                .fetch();
//        Helper.printList(exampleList5);
        
		// -----------------------------------------------------------------
        
        // Example 11 
		// Ordering 
		// What happens if we start with "from" and add ".select" under it?
//        List<Example> exampleList6 = (List<Example>) (Object) getQuerydsl().createQuery()
//                .select(exampleTable.firstName, exampleTable.lastName)
//                .from(exampleTable)
//                .orderBy(exampleTable.firstName.asc())
//                .fetch();
//        Helper.printList(exampleList6);
        
		// -----------------------------------------------------------------
        
        // Example 12
		// Grouping
//        List<Example> exampleList7 = (List<Example>) (Object) getQuerydsl().createQuery()
//                .select(exampleTable.lastName)
//                .from(exampleTable)
//                .groupBy(exampleTable.lastName)
//                //.having()
//                .fetch();
//        Helper.printList(exampleList7);
        
		// -----------------------------------------------------------------
		
        // Example 13
		// Delete
//        EntityManager em = getEntityManager();
//        em.joinTransaction();
//        long deletedItemsCount = new JPADeleteClause(em, exampleTable)
//                .where(exampleTable.lastName.eq("Brannon"))
//                .execute();
//        
//        System.out.println(deletedItemsCount + " records deleted.");

		// Now that we have potentially deleted some records, let's query the entire
        // table and print the results
//        List<Example> exampleList8 = (List<Example>) (Object) getQuerydsl().createQuery()
//                .from(exampleTable)
//                .fetch();
//        
//        for (Object result : exampleList8) {
//            System.out.println(result);
//        }

		// -----------------------------------------------------------------
		
        // Example 14
		// Update
//        long updatedCount = new JPAUpdateClause(getEntityManager(), exampleTable)
//                .set(exampleTable.lastName, "Dawg")
//                .where(exampleTable.lastName.toLowerCase().eq("sutton"))
//                .execute();
//            System.out.println(updatedCount + " records updated.");
//            printExampleTable();

		// -----------------------------------------------------------------
		
         // Example 15 
		 // Sub Query
//            Object obj = getQuerydsl().createQuery()
//                    .from(exampleTable)
//                    .where(exampleTable.id.in(
//                        JPAExpressions.select(exampleTable.id.max())
//                        .from(exampleTable)
//                        ))
//                    .fetchOne();    
//            
//            System.out.println("obj=> "  + obj);
		
	}
	
    @SuppressWarnings("unchecked")
    @Override
    public List<Object> getTables() {
        String sql = "  SELECT st.tablename " + 
        	  	     "    FROM sys.systables st " + 
        		     "   WHERE st.tablename NOT LIKE 'SYS%'" +
        		     "ORDER BY st.tablename";
        Query query = getEntityManager().createNativeQuery(sql);
        return (List<Object>)query.getResultList();
    }
	
}
