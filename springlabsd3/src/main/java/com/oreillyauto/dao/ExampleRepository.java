package com.oreillyauto.dao;

import org.springframework.data.repository.CrudRepository;

import com.oreillyauto.dao.custom.ExampleRepositoryCustom;
import com.oreillyauto.domain.Example;

public interface ExampleRepository extends CrudRepository<Example, Integer>, ExampleRepositoryCustom {
	// Add Spring Data abstract methods here	
}
