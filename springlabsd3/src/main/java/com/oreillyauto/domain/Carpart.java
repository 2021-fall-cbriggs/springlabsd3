package com.oreillyauto.domain;

import java.io.Serializable;

import javax.annotation.concurrent.Immutable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


// CARPARTS VERSION 1

//@Entity
//public class Carpart implements Serializable {
//
//	private static final long serialVersionUID = -515023023200593312L;
//	@Id
//	private String partNumber;
//	@Transient
//	private String line;
//	@Transient
//	private String title;
//	@Transient
//	private String description;
//	@Transient
//	private Map<String, String> detailsMap = new HashMap<String, String>();
//	@Transient
//	private List<Integer> storeLocations;
//	@Transient
//	private String imageName;
//
//	public Carpart() {
//	}
//
//	public String getPartNumber() {
//		return partNumber;
//	}
//
//	public void setPartNumber(String partNumber) {
//		this.partNumber = partNumber;
//	}
//
//	public String getLine() {
//		return line;
//	}
//
//	public void setLine(String line) {
//		this.line = line;
//	}
//
//	public String getTitle() {
//		return title;
//	}
//
//	public void setTitle(String title) {
//		this.title = title;
//	}
//
//	public String getDescription() {
//		return description;
//	}
//
//	public void setDescription(String description) {
//		this.description = description;
//	}
//
//	public Map<String, String> getDetailsMap() {
//		return detailsMap;
//	}
//
//	public void setDetailsMap(Map<String, String> detailsMap) {
//		this.detailsMap = detailsMap;
//	}
//
//	public List<Integer> getStoreLocations() {
//		return storeLocations;
//	}
//
//	public void setStoreLocations(List<Integer> storeLocations) {
//		this.storeLocations = storeLocations;
//	}
//
//	public String getImageName() {
//		return imageName;
//	}
//
//	public void setImageName(String imageName) {
//		this.imageName = imageName;
//	}
//
//	public static long getSerialversionuid() {
//		return serialVersionUID;
//	}
//
//}



@Entity
@Immutable
@Table(name = "CARPARTS")
public class Carpart implements Serializable {

    private static final long serialVersionUID = 4803117695298165720L;

    public Carpart() {
    }
    
    public Carpart(String partNumber, String line, String title, String description) {
		super();
		this.partNumber = partNumber;
		this.line = line;
		this.title = title;
		this.description = description;
    }

	@Id
	@Column(name = "part_number", columnDefinition = "VARCHAR(64)")
    private String partNumber;
	
	@Column(name = "line", columnDefinition = "VARCHAR(256)")
    private String line;
	
	@Column(name = "title", columnDefinition = "VARCHAR(256)")
    private String title;
	
	@Column(name = "description", columnDefinition = "VARCHAR(256)")
    private String description;
    
	@Column(name = "image_name", columnDefinition = "VARCHAR(256)")
    private String imageName;
    
    public String getPartNumber() {
        return partNumber;
    }
    
    public void setPartNumber(String partNumber) {
        this.partNumber = partNumber;
    }
    
    public String getLine() {
        return line;
    }
    
    public void setLine(String line) {
        this.line = line;
    }
    
    public String getTitle() {
        return title;
    }
    
    public void setTitle(String title) {
        this.title = title;
    }
    
    public String getDescription() {
        return description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }
    
    public String getImageName() {
        return imageName;
    }
    
    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

	@Override
	public String toString() {
		return "Carpart [partNumber=" + partNumber + ", line=" + line + ", title=" + title + ", description="
				+ description + "]";
	}
    
}

