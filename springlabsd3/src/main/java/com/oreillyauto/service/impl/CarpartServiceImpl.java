package com.oreillyauto.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.oreillyauto.dao.CarpartsRepository;
import com.oreillyauto.domain.Carpart;
import com.oreillyauto.service.CarpartsService;

@Service 
public class CarpartServiceImpl implements CarpartsService {
	@Autowired
	private CarpartsRepository carpartsRepo;

    @Override
    public Carpart getCarpartByPartNumber(String partNumber) {
    	// V1
//    	return null;
    	
    	// V2
//    	return carpartsRepo.getCarpartByPartNumber(partNumber);
    	
    	// V3
    	Optional<Carpart> opt = carpartsRepo.findById(partNumber);
    	return opt.isPresent() ? opt.get() : null; 
    }
    
}

